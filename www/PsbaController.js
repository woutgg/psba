"use strict";

function PsbaController() {
	var MAX_FETCH_LINES = 1000;
	var AJAX_TIMEOUT = 5000;
	var LOG_LIST_DATA_EV = 'log-list-data';
	var LOG_DATA_EV = 'log-data';

	//var self = this;
	var ajaxUrl = window.location;

	var logList = null;
	var visualizer = null;


	/* exported functions */

	this.init = function(visualizerObj) {
		visualizer = visualizerObj;
		$(document).bind(LOG_LIST_DATA_EV, logListLoaded);
		$(document).bind(LOG_DATA_EV, newLogData);
		requestLogList();

		setStatus("List of log files requested...", "ok");
	}


	/* private scope */

	function requestLogList() {
		$.ajax({
			url: ajaxUrl,
			type: 'GET',
			timeout: AJAX_TIMEOUT,
			/*context: this,*/
			data: { op: 'list' },
			success: function(data, status, xhr) {
				$(document).trigger(LOG_LIST_DATA_EV, [ data.status, data.status == "success" ? data.data : data.message ]);
			},
			error: function(xhr, status) {
				$(document).trigger(LOG_LIST_DATA_EV, [ status, null ]);
				console.log("AJAX error (log list request): " + status);
			}
		});
	}

	//TODO: load chunks if logList[idx].lines is larger than 1000 (adapt newLogData to accept chunks)
	function requestLogData(idx) {
		var start = 0;
		var total = logList[idx].lines;
		var len = total > MAX_FETCH_LINES ? MAX_FETCH_LINES : total;

		$.ajax({
			url: ajaxUrl,
			type: 'GET',
			timeout: AJAX_TIMEOUT,
			data: { op: 'get', idx: idx },
			success: function(data, status, xhr) {
				$(document).trigger(LOG_DATA_EV, [ data.status,
						data.status == "success" ? data.data : data.message,
						{ idx: idx, start: start, len: len }
				]);
			},
			error: function(xhr, status) {
				$(document).trigger(LOG_DATA_EV, [ status, null, { idx: idx, start: start, len: len } ]);
				console.log("AJAX error (log data request): " + status);
			}
		});
	}


	function logListLoaded(ev, status, data) {
		if (status == "error") {
			if (data) setStatus("Could not load log file list (" + data + ")", "error");
			else setStatus("Could not load log file list", "error");
			return;
		}

		logList = data;

		var max = 0, count = 0;
		for (var idx in logList) {
			count++;
			var nidx = parseInt(idx);
			if (nidx > max) max = nidx;
		}

		if (count > 0) {
			console.log("logListLoaded(): last log file: " + max + ", size: " + logList[max].size + ", lines: " + logList[max].lines);
			setStatus("Log file list loaded (" + count + " files available, loading last one (#" + max + ")", "ok");
		} else {
			console.log("logListLoaded(): no log files available");
			setStatus("Log file list loaded (no files available)", "ok");
		}
		requestLogData(max);
	}

	function newLogData(ev, status, data, metadata) {
		if (status == "error") {
			if (data) setStatus("Could not load log data for index " + metadata.idx + " (" + data + ")", "error");
			else setStatus("Could not load log data for index " + metadata.idx, "error");
			return;
		}

		setStatus("Loaded log data for index " + metadata.idx + " (" + metadata.len + " lines)", "ok");

		console.log("newLogData: loaded index " + metadata.idx + " (start=" + metadata.start + ", length=" + metadata.len + ")");
		visualizer.addData(data, metadata);
		visualizer.render();
	}
}
