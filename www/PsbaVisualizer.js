"use strict";

function PsbaVisualizer() {
	/* CONSTANTS */

	var SCALE_COLOR = 'black';
	var SCALE_LWIDTH = 1;
	var BALL_COUNT_GRAPH_COLOR = 'blue';
	var BALL_COUNT_GRAPH_LWIDTH = 2;
	var REVERSE_PAINT_COLOR = '#8f4';
	var MAGNET_VALVE_LINE_COLOR = '#f55';
	var MAGNET_VALVE_LINE_WIDTH = 1;
	var INST_ACTIVE_OVERLAY_COLOR = '#666';
	var INST_ACTIVE_OVERLAY_ALPHA = 0.9;
	var ATTRACT_MODE_LINE_COLOR = '#077';
	var ATTRACT_MODE_LINE_WIDTH = 2;
	var ATTRACT_MODE_DASH_LEN = 5;

	//margins around the actual graphing area (mostly required for axis annotations and legend)
	var MARGINS = { left: 35, top: 5, right: 5, bottom: 120 };


	/* VARIABLES */

	var canvas = null;
	var ctx = null;

	//vars for scrolling/zooming
	var xOffset = 0;
	var zoomScale = 1;

	var logData = {};


	/* PUBLIC FUNCTIONS */

	this.init = function(canvasId) {
		canvas = $('#' + canvasId)[0];
		ctx = canvas.getContext('2d');

		ctx.canvas.width = $(document).width();
		ctx.canvas.height = $(document).height() - $('#status_bar').height();
		ctx.textAlign = "left";
	}

	this.getXOffset = function() { return xOffset; }
	this.setXOffset = function(xOffs) { xOffset = xOffs; }
	this.changeXOffset = function(xDelta) { xOffset += xDelta; }

	this.getZoom = function() { return zoomScale; }
	this.setZoom = function(z) { zoomScale = z; }
	this.changeZoom = function(zoomDelta) { zoomScale += zoomDelta; }

	this.clearData = function() {
		data = {};
	}

	//we only need index as metadata, start is implied by the first line number and length is just the amount of entries
	this.addData = function(data, metadata) {
		if (logData[metadata.idx] == null) {
			setStatus("Set data for file index " + metadata.idx, "ok");
			logData[metadata.idx] = data;
		} else {
			setStatus("Nah, sorry... appending data for existing index not implemented", "warning");
			//TODO: search backwards for the largest line number < (first found in new data) and insert after that
		}
	}

	this.render = function() {
		//setStatus("Rendering...", "ok");
		var renderIdx = 49; //TEMP


		/* dump log data to HTML below canvas */
		var block = logData[renderIdx].lines;
		var txt = "";
		for (var lnum in block) {
			var l = block[lnum];
			var ts = parseInt(l.ts);

			//txt += "ts: " + l.ts + "; tag: " + l.tag + "; tag_args: [NI]; msg: " + l.msg + "<br>";
			txt += "line " + lnum + ": " + JSON.stringify(l) + "<br>";
		}
		$('#raw_data').html("<pre>" + txt + "<pre>");


		console.time("render frame");

		var dims = getAutoDimensions(renderIdx);
		console.log("auto dimensions: " + JSON.stringify(dims));

		ctx.clearRect(0, 0, dims.width, dims.height);

		dims.ga.x += xOffset;
		dims.ga.scale_x *= zoomScale;
		console.log("auto dimensions (scroll/zoom applied): " + JSON.stringify(dims));

		drawReversingPaint(dims, renderIdx);
		drawMagnetValveLines(dims, renderIdx);
		drawBallCountGraph(dims, renderIdx);
		drawInstallationActiveOverlay(dims, renderIdx);
		drawAttractModeTriggerLines(dims, renderIdx);
		drawScales(dims, false);
		drawLegend(dims);
		console.timeEnd("render frame");
	}


	/* PRIVATE FUNCTIONS */

	function getAutoDimensions(idx) {
		var block = logData[idx].lines;
		var maxMSecs = 0;
		var maxCount = 0;

		for (var lnum in block) {
			var l = block[lnum];
			var ts = parseInt(l.ts);
			var count = (l.tag == "LBct") ? parseInt(l.tag_args[0]) : 0;

			if (maxCount < count) maxCount = count;
			if (maxMSecs < ts) maxMSecs = ts;
		}

		var dims = { width: canvas.width, height: canvas.height, margins: MARGINS };

		var areaW = dims.width - MARGINS.left - MARGINS.right;
		var areaH = dims.height - MARGINS.top - MARGINS.bottom;

		var ga = { x: MARGINS.left, y: MARGINS.top, width: areaW, height: areaH, scale_x: areaW / maxMSecs, scale_y: areaH / maxCount };
		dims.ga = ga;

		console.log("getAutoDimensions: canvas: (" + dims.width + ", " + dims.height + "); maxMSecs: " + maxMSecs + "; maxCount: " + maxCount +
				" => scale: (" + dims.ga.scale_x + ", " + dims.ga.scale_y + ")");

		return dims;
	}

	function drawScales(dims, skipVertical = false) {
		drawScaleLine(dims, 20, 'hor', function(v) {
			return prettyPrintTime(v / dims.ga.scale_x);
		});

		if (!skipVertical) {
			drawScaleLine(dims, 10, 'ver', function(v) {
				return Math.round(v / dims.ga.scale_y);
			});
		}
	}

	function drawScaleLine(dims, steps, dir, labelFunc) {
		var isHor = (dir == 'hor');

		ctx.save();
		ctx.textAlign = "right";

		var step, mlen;
		if (isHor) {
			step = (dims.ga.width - 15) / (steps - 1); //subtract 15 and calc with 'one point less' so everything fits nicely in the graph area
			mlen = dims.ga.height / 100;
		} else {
			step = (dims.ga.height - 15) / (steps - 1); //subtract 15 and calc with 'one point less' so everything fits nicely in the graph area
			mlen = dims.ga.width / 100;
		}
		if (mlen < 10) mlen = 10;

		ctx.strokeStyle = SCALE_COLOR;
		ctx.fillStyle = SCALE_COLOR; //for text
		ctx.lineWidth = SCALE_LWIDTH;

		if (isHor) ctx.drawLine(dims.ga.x, dims.ga.y + dims.ga.height, dims.ga.x + dims.ga.width, dims.ga.y + dims.ga.height, SCALE_COLOR, SCALE_LWIDTH);
		else ctx.drawLine(dims.ga.x, dims.ga.y + dims.ga.height, dims.ga.x, dims.ga.y, SCALE_COLOR, SCALE_LWIDTH);

		for (var i = 0; i < steps; ++i) {
			var varCoord = Math.round(isHor ? dims.ga.x + step * i : dims.ga.y + dims.ga.height - 1 - step * i);

			ctx.beginPath();
			if (isHor) {
				ctx.moveTo(varCoord, dims.ga.y + dims.ga.height - 1);
				ctx.lineTo(varCoord, dims.ga.y + dims.ga.height - mlen);
			} else {
				ctx.moveTo(dims.ga.x, varCoord);
				ctx.lineTo(dims.ga.x + mlen, varCoord);
			}
			ctx.stroke();
			ctx.closePath();

			ctx.save();
			if (isHor) {
				ctx.translate(varCoord + 3, dims.ga.y + dims.ga.height + 4);
				ctx.rotate(-Math.PI / 2);
				ctx.fillText(labelFunc(varCoord), 0, 0);
			} else {
				ctx.translate(dims.ga.x - 3, varCoord);
				ctx.fillText(labelFunc(dims.ga.y + dims.ga.height - varCoord), 0, 0);
			}
			ctx.restore();
		}

		ctx.restore();
	}

	function drawBallCountGraph(dims, idx) {
		ctx.save();
		var block = logData[idx].lines;

		var lastX = dims.ga.x, lastY = dims.y + dims.ga.height;

		ctx.beginPath();
		ctx.strokeStyle = BALL_COUNT_GRAPH_COLOR;
		ctx.lineWidth = BALL_COUNT_GRAPH_LWIDTH;
		ctx.moveTo(lastX, lastY);
		for (var lnum in block) {
			var l = block[lnum];
			if (l.tag != "LBct") continue;

			var ts = parseInt(l.ts);
			var count = parseInt(l.tag_args[0]);

			var newX = dims.ga.x + ts * dims.ga.scale_x, newY = dims.ga.y + dims.ga.height - count * dims.ga.scale_y;
			ctx.lineTo(newX, newY);

			lastX = newX; lastY = newY;
		}

		ctx.stroke();
		ctx.closePath();
		ctx.restore();
	}

	function drawReversingPaint(dims, idx) {
		ctx.save();
		var block = logData[idx].lines;
		var top = dims.ga.y, bottom = dims.ga.y + dims.ga.height;

		var lastDir = true, lastX = dims.ga.x;

		ctx.fillStyle = REVERSE_PAINT_COLOR;
		for (var lnum in block) {
			var l = block[lnum];
			if (l.tag != "RDen") continue;

			var ts = parseInt(l.ts);
			var dir = !lastDir, newX = dims.ga.x + ts * dims.ga.scale_x;

			if (lastDir) ctx.fillRect(lastX, top, newX - lastX, bottom - top);

			lastDir = dir;
			lastX = newX;
		}

		ctx.restore();
	}

	function drawMagnetValveLines(dims, idx) {
		ctx.save();
		var block = logData[idx].lines;
		var top = dims.ga.y, bottom = dims.ga.y + dims.ga.height;

		for (var lnum in block) {
			var l = block[lnum];
			if (l.tag != "MVop") continue;

			var ts = parseInt(l.ts);
			var x = dims.ga.x + ts * dims.ga.scale_x;

			ctx.drawLine(x, top, x, bottom, MAGNET_VALVE_LINE_COLOR, MAGNET_VALVE_LINE_WIDTH);
		}

		ctx.restore();
	}

	function drawInstallationActiveOverlay(dims, idx) {
		ctx.save();
		var block = logData[idx].lines;
		var top = dims.ga.y, bottom = dims.ga.y + dims.ga.height;

		var lastState = false, lastX = dims.ga.x;

		ctx.fillStyle = INST_ACTIVE_OVERLAY_COLOR;
		ctx.globalAlpha = INST_ACTIVE_OVERLAY_ALPHA;
		for (var lnum in block) {
			var l = block[lnum];

			var state;
			if (l.tag == "I1en") state = true;
			else if (l.tag == "I0en") state = false;
			else continue;

			var ts = parseInt(l.ts);
			var newX = dims.ga.x + ts * dims.ga.scale_x;
			//console.log("inst on/off - ts: " + ts + ", state: " + state + ", X: " + newX);

			if (!lastState) ctx.fillRect(lastX, top, newX - lastX, bottom - top); //grey out data which covers inactive installation (i.e. up till 'rising' edge)

			lastState = state;
			lastX = newX;
		}

		ctx.restore();
	}

	function drawAttractModeTriggerLines(dims, idx) {
		ctx.save();

		var block = logData[idx].lines;
		var top = dims.ga.y, bottom = dims.ga.y + dims.ga.height;

		for (var lnum in block) {
			var l = block[lnum];
			if (l.tag != "AMen") continue;

			var ts = parseInt(l.ts);
			var x = dims.ga.x + ts * dims.ga.scale_x;
			//console.log("attract - ts: " + ts + " x: " + x);

			ctx.dashedLine(x, top, x, bottom, ATTRACT_MODE_LINE_COLOR, ATTRACT_MODE_LINE_WIDTH, ATTRACT_MODE_DASH_LEN);
		}

		ctx.restore();
	}

	function drawLegend(dims) {
		var lineHeight = 11;
		var orgY = dims.height - 6 * lineHeight, vlc = lineHeight / 3;
		var x = 20, y = orgY;
		var maxWidth = 0; //drawText() keeps track of longest line here

		function drawText(text, x, y) {
			ctx.fillText(text, x + 30, y);
			var w = ctx.measureText(text).width;
			if (w > maxWidth) maxWidth = w;
		}

		ctx.save();

		//Left column

		ctx.fillText("Legend:", x, y);
		y += lineHeight;

		ctx.drawLine(x, y - vlc, x + 25, y - vlc, BALL_COUNT_GRAPH_COLOR, BALL_COUNT_GRAPH_LWIDTH);
		drawText("Amount of balls counted", x + 30, y);
		y += lineHeight;

		ctx.drawLine(x, y - vlc, x + 25, y - vlc, MAGNET_VALVE_LINE_COLOR, MAGNET_VALVE_LINE_WIDTH);
		drawText("Magnet valve activated", x + 30, y);
		y += lineHeight;

		ctx.dashedLine(x, y - vlc, x + 25, y - vlc, ATTRACT_MODE_LINE_COLOR, ATTRACT_MODE_LINE_WIDTH, ATTRACT_MODE_DASH_LEN);
		drawText("Attract mode activated", x + 30, y);
		y += lineHeight;

		//Right column
		y = orgY + lineHeight; //back to top, but leave 'Legend:' line empty
		x += 30 + maxWidth + 60; //+60 is for some spacing as well as to compensate for metrics being too small

		ctx.fillText("* Green/white alterations indicate revresing of air flow", x, y);
		y += lineHeight;

		ctx.fillText("* The installation was not switched on in semi-transparent areas", x, y);
		y += lineHeight;

		ctx.restore();
	}


	/* (utility functions) */

	function prettyPrintTime(msecs) {
		var msPart = msecs % 1000;
		var sPart = Math.floor((msecs % 60000) / 1000);
		var mPart = Math.floor((msecs % (60 * 60000)) / 60000);

		if (msecs < 1000) return msPart + "ms";
		else if (msecs < 60 * 1000) return sPart + "s";
		else if (msecs < 60 * 60000) return mPart + "m" + sPart + "s";
		else return Math.floor(msecs / (60 * 60000)) + "h" + mPart + "m"  + sPart + "s";
	}

	CanvasRenderingContext2D.prototype.drawLine = function(x1, y1, x2, y2, strokeStyle, lineWidth) {
		this.save();
		if (strokeStyle != null) this.strokeStyle = strokeStyle;
		if (lineWidth != null) this.lineWidth = lineWidth;

		this.beginPath();
		this.moveTo(x1, y1);
		this.lineTo(x2, y2);
		this.stroke();
		this.closePath();
		this.restore();
	}

	//adapted from: https://stackoverflow.com/a/15968095
	CanvasRenderingContext2D.prototype.dashedLine = function(x1, y1, x2, y2, strokeStyle, lineWidth, dashLen) {
		if (strokeStyle != null) ctx.strokeStyle = strokeStyle;
		if (lineWidth != null) ctx.lineWidth = lineWidth;
		if (dashLen == undefined) dashLen = 2;

		ctx.beginPath();
		this.moveTo(x1, y1);

		var dX = x2 - x1;
		var dY = y2 - y1;
		var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
		var dashX = dX / dashes;
		var dashY = dY / dashes;

		var q = 0;
		while (q++ < dashes) {
			x1 += dashX;
			y1 += dashY;
			this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
		}
		this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
		ctx.stroke();
		ctx.closePath();
	};
}
