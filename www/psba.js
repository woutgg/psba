"use strict";

//'main' function
$(document).ready(function() {
	var orgSBHeight = $('#status_bar').height();

	var controller = new PsbaController();
	var visualizer = new PsbaVisualizer();
	visualizer.init('canvas');
	controller.init(visualizer);


	//vars for keeping track of mouse dragging
	var dragging = false;
	var mouseStart = { x: 0, y: 0 };


	$('#canvas').mousedown(function(ev) {
		dragging = true;
		mouseStart.x = ev.pageX;
		mouseStart.y = ev.pageY;
	});

	$('#canvas').mouseup(function(ev) {
		dragging = false;
	});

	$('#canvas').mousemove(function(ev) {
		if (!dragging) return;

		var dx = ev.pageX - mouseStart.x;
		var dy = ev.pageY - mouseStart.y;
		mouseStart.x = ev.pageX;
		mouseStart.y = ev.pageY;
		visualizer.changeXOffset(dx);
		visualizer.render();

		//console.log("mouse delta: (" + dx + ", " + dy + ")");
	});

	$('#canvas').dblclick(function(ev) {
		setStatus("Zoom and scroll reset");
		visualizer.setXOffset(0);
		visualizer.setZoom(1);
		visualizer.render();
	});


	$('#zoom_out').click(function(ev) {
		visualizer.changeZoom(-.1);
		visualizer.render();
	});

	$('#zoom_reset').click(function(ev) {
		visualizer.setZoom(1);
		visualizer.render();
	});

	$('#zoom_in').click(function(ev) {
		visualizer.changeZoom(.1);
		visualizer.render();
	});


//Note: this will only work if the div would clip to the bottom
/*
	$('#status_bar').on('click', function(ev) {
		var o = $(this);
		var h = o.height() > orgSBHeight ? orgSBHeight : orgSBHeight * 10;
		$(this).height(h);
	});
*/
});


/* UTILITIES */

function setStatus(text, color) {
	if (color == 'ok') color = 'white';
	else if (color == 'warning') color = 'yellow';
	else if (color == 'error') color = '#f55';
	else if (color == undefined) color = 'white';

	$('#status_bar').text(text).css('color', color);

	//Note: appending text is only useful in conjunction with collapsing (see $(document).ready())
/*
	var otext = $('#status_bar').html();
	if (otext.length > 0) otext += "<br>\n";
	$('#status_bar').html(otext + text).css('color', color);
*/
}
