<?php

/*
 * This file handles 1) uploading log messages from the installation to a text file on the server,
 * 2) dishing out a simple HTML page which uses a HTML5 canvas and psba.js to visualize log data and
 * 3) responding to AJAX requests made by psba.js to provide it with the log data that resides on the server.
 */

/*
ini_set('display_startup_errors',1);
ini_set('display_errors', 1);
error_reporting(-1);
*/

define('LOG_DIR', 'logs');
define('LOG_FILE_NAME', 'post-log.txt');
define('LOG_FILE_EXTENSION', '.LOG');
define('GET_DEFAULT_MAX_LINES', 1000);

$html = <<<EOT_HTML
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Pneumatic Sponge Ball Accelerator Analyzer</title>
		<!--script src="https://code.jquery.com/jquery-2.1.1.min.js"></script-->
		<script src="jquery-2.1.1.min.js"></script>
		<script src="PsbaVisualizer.js"></script>
		<script src="PsbaController.js"></script>
		<script src="psba.js"></script>
		<link rel="stylesheet" href="psba.css">
	</head>
	<body>
	<div id="buttons">
		[mind you, this is just a work in progress / playground...]
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		//
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		(drag left/right to scroll, double-click to reset)
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		//
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Zoom:</b>
		<span id="zoom_out">-</span>
		<span id="zoom_reset">0</span>
		<span id="zoom_in">+</span>
	</div>
	<div id="canvas_container">
		<canvas id="canvas" width="800" height="300"></canvas>
	</div>
	<div id="raw_data"></div>
	<div id="status_container"><div id="status_bar"></div></div>
</body>
</html>
EOT_HTML;


//print_r($_REQUEST, TRUE);
$op = isset($_REQUEST['op']) ? $_REQUEST['op'] : NULL;

//main switch for choosing which action to take
if ($op == 'log') { //save a new log message to the text file
	$msg = getRqArgument('msg');
	$ts = getRqArgument('ts');

	$line = date('Y-m-d H:i:s') . "\t" . $ts . "\t" . $msg;

	if (file_exists(LOG_FILE_NAME) && !is_writable(LOG_FILE_NAME)) {
		ajaxExitError("cannot write to file.");
	}

	$rv = file_put_contents(LOG_FILE_NAME, $line . "\n", FILE_APPEND);

	if ($rv !== false) {
		ajaxExitOk();
		//print("OK written " . $rv . " bytes to the logfile:<pre style=\"background-color: #eee;\">" . $line . "</pre>");
	} else {
		ajaxExitError();
		//ajaxExitError("ERR error writing data to log file");
	}

} elseif ($op == 'list') { //list all log files (numbered files copied from sd card)
	$hnd = opendir(LOG_DIR);

	if (!$hnd) {
		ajaxExitError("");
	}

	$list = array();
	$lfeLen = strlen(LOG_FILE_EXTENSION);
	$lfeLCase = strtolower(LOG_FILE_EXTENSION);
	while (false !== ($ent = readdir($hnd))) {
		$filename = LOG_DIR . '/' . $ent;
		$proc = strtolower($ent);

		if (!strrpos($proc, $lfeLCase) == strlen($proc) - $lfeLen) continue;

		$proc = substr($proc, 0, strlen($proc) - $lfeLen);

		if (!is_numeric($proc)) continue;

		$idx = intval($proc);
		$lc = countLines($filename);

		$list[$idx] = array( 'size' => filesize($filename), 'lines' => $lc );
	}

	closedir($hnd);
	ajaxExitOk($list);

} elseif ($op == 'get') { //retrieve (part of) a log file (numbered sd card file)
	$idx = getRqArgument('idx');
	$start = intval(getRqArgument('start', 0));
	$len = intval(getRqArgument('len', GET_DEFAULT_MAX_LINES)); //allow large numbers if passed explicitly
	$concise = toBool(getRqArgument('concise', 'false'));

	if ($idx == null) ajaxExitError("missing argument: 'idx'");

	$filename = LOG_DIR . '/' . str_pad($idx, 5, '0', STR_PAD_LEFT) . LOG_FILE_EXTENSION;
	$fullFile = true;

	if (!file_exists($filename)) ajaxExitError("no such index");

	$hnd = fopen($filename, 'r');
	if (!$hnd) ajaxExitError("could not open file '" . $filename . "'");

	$lines = array();
	$counter = -1;
	while (!feof($hnd)) {
		$counter++;
		$line = rtrim(fgets($hnd));

		if (strlen($line) == 0) {
			$counter--;
			break;
		}

		if ($counter < $start) continue;

		$tabpos = strpos($line, "\t");
		$ts = intval(substr($line, 0, $tabpos));
		$tag = '';
		$msg = substr($line, $tabpos + 1);

		$brOpen = strpos($msg, "[");
		$brClose = strpos($msg, "]");
		$tagArgs = null;
		if ($brOpen !== false && $brClose !== false && $brClose > $brOpen) {
			$tag = substr($msg, $brOpen + 1, $brClose - $brOpen - 1);
			$msg = substr($msg, $brClose + 1);

			$tagArgs = explode(',', $tag);
			if ($tagArgs !== false) $tag = array_shift($tagArgs);
		}

		$msg = trim($msg);

		$lineArr = array( 'ts' => $ts, 'tag' => $tag, 'msg' => $msg );
		if (is_array($tagArgs) && count($tagArgs)) $lineArr['tag_args'] = $tagArgs;

		if (!$concise) $lines["$counter"] = $lineArr;
		else $lines["$counter"] = array( 'ts' => $ts, 'tag' => $tag );

		if ($counter - $start >= $len - 1) {
			$fullFile = false;
			break;
		}
	}

	fclose($hnd);
	ajaxExitOk(array('full_file' => $fullFile, 'lines' => $lines));

} elseif (isset($op)) { //operation specified, but not one we know...
	ajaxExitError("Unknown operation: '$op'");

} else { //no op given, show the visualizer itself
/*
	$log = @file_get_contents(LOG_FILE_NAME);

	if ($log !== false) print("<pre>" . $log . "</pre>");
	else print("could not open log file.");
*/
	echo $html;
}


function getRqArgument($argName, $dfl = null) {
	return isset($_REQUEST[$argName]) ? $_REQUEST[$argName] : $dfl;
}

function toBool($val) {
	return !($val == null || $val === false || (is_numeric($val) && $val == 0) || $val == '0' || stripos($val, 'f') !== false || stripos($val, 'false') !== false);
}

//adapted from https://stackoverflow.com/a/2162528
//FIXME: should not count empty last line, as this is not returned by 'get' either
function countLines($file) {
	$lc = 0;
	$handle = fopen($file, 'r');
	while (!feof($handle)) {
		$line = fgets($handle, 4096);
		$lc += substr_count($line, PHP_EOL);
	}

	fclose($handle);
	return $lc;
}


function ajaxExitOk($data) {
	$arr = array( 'status' => 'success' );

	if ($data !== null) $arr['data'] = $data;

	header('Content-type: application/json');
	echo json_encode($arr, JSON_FORCE_OBJECT);
	//echo json_encode($arr, JSON_FORCE_OBJECT | JSON_NUMERIC_CHECK);
	exit(0);
}

function ajaxExitError($msg = null, $data = null) {
	$arr = array( 'status' => 'error' );
	if ($msg !== null) $arr['message'] = $msg;
	if ($data !== null) $arr['data'] = $data;

	header('Content-type: application/json');
	echo json_encode($arr);
	exit(1);
}

?>
