Pneumatic Sponge Ball Accelerator - by Niklas Roy

During summer 2014, it could be experienced at the Tschumipaviljoen, Groningen.


Notes:
- logs.tar.xz contains log files of the installation's activity in Groningen.
- the sensorcode directory contains AVR code used in the hand sensor.
- the www directory contains (partially broken) web code to render visualizations from the log files.
