//Tschumi Pavillon / Sensor
//Runs seperately from the main controller on an Atmega8 with 16MHz crystal
//Released under a "I-know-that-code-is-badly-documented-but-at-least-it-works-so-do-whatever-you-want-with-it" - license

//main controller is connected to PORTB0
//LED's are connected to PORTB2
//RGB-LED spotlight remote control is connected via relais to PORTB1(green) and PORTB2(red)
//LDR is connected to ADC0
//Adjustment Poti to ADC1
//A piezo which beeps when the sensor is activated is connected to PORTD3 

//SERIAL ECHO
#define F_CPU 16000000UL  // 16 MHz
#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <util/delay.h>

#define LEDON  PORTB |=  (1 << 2); 
#define LEDOFF PORTB &= ~(1 << 2);
#define OUTHI  PORTB |=  (1 << 0); PORTB |=  (1 << 1); 
#define OUTLO  PORTB &= ~(1 << 0); PORTB &= ~(1 << 1);

uint8_t old_state=0;

//////////////////////////////////////////////////////////////// MAIN /////////////////////////////

int main(void)
{

	//IO
	DDRB = 0b00000111;  					// b0:Output - b1:green LED - b2:red LED's
	DDRC = 0b00000000;  					// ADC0:LDR - ADC1:POTI
	DDRD = 0b11001000;  					// D3:Piezo D6,D7 LED Spot Color 

	//Pull Up's / Outputs
	PORTB= 0b00000111;
	PORTC= 0b00000000;
	PORTD= 0b00000000;

	UCSRB=0b00011000; 						// UART on and configure
	UCSRC=0b10000110; 						// UART config: 8N1, asynchronous
	UBRRH=0x00;		 						// Baud frequency: 9600Bd @ 16MHz
	UBRRL=0x67; 							// """"""""""""""""""""""""""""""
	ADMUX=0b00100000;						// ADC Channel Muliplexer
	ADCSRA=0b11100100;						// ADC Timer Division 16
	
	PORTD |=  (1 << 6);
	for (int b=0;b<100;b++){
		PORTD |=  (1 << 3);
		_delay_ms(.25);
		PORTD &= ~(1 << 3);
		_delay_ms(.25);
	}
	_delay_ms(500);
	PORTD &= ~(1 << 6);

	while(1)
	{
		
		ADMUX=0b00100100;  // ADC Channel: PC4 (POTI)
		uint8_t poti=ADCH;_delay_ms(1);poti=ADCH; // reading POTI
				
		LEDOFF
		_delay_ms(15);
		
		ADMUX=0b00100101;  // ADC Channel: PC5 (LDR)
		uint8_t ldr0=ADCH;_delay_ms(1);ldr0=ADCH; // reading LDR bright=40 -- dark=220
	
		LEDON
		_delay_ms(10);
	
		ADMUX=0b00100101;  // ADC Channel: PC5 (LDR)
		uint8_t ldr1=ADCH;_delay_ms(1);ldr1=ADCH; // reading LDR bright=40 -- dark=220
			
		uint8_t difference=0;
		if (ldr0>ldr1) difference=ldr0-ldr1;
		if (difference>(poti/2+1)) {
			OUTHI //LED & OUTPUT HIGH
			if (old_state==0){
				 PORTD |=  (1 << 7); 
				 for (int b=0;b<100;b++){
					 PORTD |=  (1 << 3); 
					 _delay_ms(.5); 
					 PORTD &= ~(1 << 3);
					 _delay_ms(1); 
					 }
			     _delay_ms(200);
				 PORTD &= ~(1 << 7);  	
				 }
			old_state=1;
			}  
		if (difference<poti/2) 
		{
			OUTLO //LED & OUTPUT LOW
			if (old_state==1) {
				PORTD |=  (1 << 6);
				for (int b=0;b<100;b++){
					PORTD |=  (1 << 3); 
					_delay_ms(1); 
					PORTD &= ~(1 << 3);
					_delay_ms(1); 
					} 
					
				_delay_ms(200);
				PORTD &= ~(1 << 6);
				}
			old_state=0;
		}						
									
	}
}