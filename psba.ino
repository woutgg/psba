/*
   --- Arduino Version 1.5.5
   --- Pneumatic Particle Accelerator
   --- Version 1.1
   --- Tschumi Paviljoen Groningen
 */

/*
 * NOTES:
 * - ref: http://arduino.cc/en/Tutorial/WebClientRepeating
 * - ref: http://arduino.cc/en/Tutorial/WebServer
 * - ref: http://arduino.cc/en/Reference/SDCardNotes
 * - log messages are prefixed with an identifier for (future) automatic parsing, formatted as follows: '[FNop]'
 *    where FN are two chars identifying the function, op identifying the specific log message, and then optionally comma separated 'arguments' can follow
 *
 * ethernet issues:
 * - http://forum.arduino.cc/index.php/topic,28084.0.html (2009)
 * - https://github.com/arduino/Arduino/pull/1240 (2013)
 *
 * TODO:
 * - re-introduce direction reversing in attract mode (although less 'thorough'?) and also use it in the end sequence
 * - ethernet connect seems to take ages on connect when it cannot connect (spi device absent).
 *   if this cannot be avoided, the connect should only be run in setup once, to make sure the installation will always run well
 * - try opening sd file always when it's not open, so it will be 'hot pluggable'?
 * - for more optimal code with String class, use: -felide-constructors,-std=c++0x (as per WString.h in Arduino source)
 */

/*
 * TEST_MODEs:
 * - comment out for normal operation;
 * - define to 1 to run 10secs - stop - reverse - wait 2secs - repeat;
 * - define to 2 to run continuously and switch direction every 10 seconds
 * - define to 3 to run in one direction continuously (touching the hand sensor for at least 10 seconds will reverse air direction).
 */
//#define TEST_MODE 2

//Uncomment this to enable network code
//#define NETWORK_ENABLED

#define LOG_DIR "ppa-logs" /* where to store log files on the SD-card */


#include <String>

#ifndef FLASH_H_SEEN /* include protection guard kludge because otherwise the Arduino compile magic messes stuff up */
# include "Flash.h" /* Note: Flash.cpp and Flash.h are from http://arduiniana.org/libraries/flash/  */
# define FLASH_H_SEEN
#endif

#include <SPI.h>

#ifdef NETWORK_ENABLED
#include <Ethernet.h>
#endif

#include <SD.h>
#include "psba.h"



#ifdef NETWORK_ENABLED
const uint8_t MAC_ADDRESS[] = { 0xED, 0xFE, 0xFE, 0xFE, 0xAD, 0xDE }; //ehmm deadbeeffeed anagram...it doesn't matter anyhow
const IPAddress IP_ADDRESS(192, 168, 1, 100); //static address outside dhcp pool
const IPAddress DNS_SERVER(192, 168, 1, 1); //tschumi router forwards DNS?
const char *SERVER_HOST = "lsof.nl";

EthernetClient client;
#endif



//pinmapping
const int airDirectionSensor   = 0;
const int handSensor           = 1;
const int relais1              = 2;
const int relais2              = 3;
const int relais3              = 5;
const int magnetValve          = 6;
const int stirMotorA           = 7;
const int stirMotorB           = 8;
const int airDirectionMotor    = 9;
const int lightBarrierPin     = 16;

//constants for adjusting installation
const unsigned long autoSwitchOffDelay  =      60 * 1000L; //60000L; defines the delay (in milliseconds) after which the installation switches itself off after the sensor has been activated for the last time
const unsigned long magnetValveInterval =           9000L; //17000L; defines how often the magnet valve opens in milliseconds
const unsigned long vandalismTimeOut    =         150000L; //150000L; defines after which time the installation switches off when sensor stays permanently high
const unsigned long attractModeInterval =     360 * 1000L; //600000L; defines how long after last activity the attract mode is started
const unsigned long ATTRACT_MODE_DURATION =    45 * 1000L;
const unsigned long ATTRACT_MODE_AIR_REV_DELAY =   12000L; //must be smaller than ATTRACT_MODE_DURATION, or set equal/larger to disable reversing
const unsigned long ATTRACT_MODE_AIR_REV_MIN_ADD =  4000L; //minimum delay for next reverse after first one
const unsigned long ATTRACT_MODE_AIR_REV_MAX_ADD =  9000L; //maximum delay for next reverse after first one
const unsigned long MAGNET_VALVE_BURST_TIME =         45L; //3 short bursts
const unsigned long MAGNET_VALVE_OPEN_TIME =        1000L; //time the valve will be opened after the bursts
const unsigned long LOG_FLUSH_DELAY =               5000L;
const unsigned long LIGHT_BARRIER_TIMEOUT =    60 * 1000L; //log a message if light barrier did not switch after this timeout while installation is on
const int LIGHT_BARRIER_THRESHOLD =                   500; //noise varies between about 200-350 (but sometimes also much higher?), value when ball passes is about 800-900

//global variables
unsigned long autoSwitchOffTimer = 0;
unsigned long magnetValveTimer   = 0;
unsigned long vandalismTimer     = 0;
unsigned long attractModeTimer   = 0;
unsigned long lastSensorToggle   = 0;
bool pHandSensor                 = false;
int airFlowDirection             = 1;
int x=0;
bool installationOn = false;
unsigned long ballCounter = 0;

File logFile;

//forward declarations - somehow the NETWORK_ENABLED define (when disabled) breaks Arduino's auto decls...
bool initSdCard();
void attractMode();
bool readHandSensor();
void magnetValveRoutine();
void reverseAirDirection();
void switchInstallationOn();
void switchInstallationOff();
void checkLightBarrier();
bool interruptableDelay(unsigned long msecs);
void logMessage(const String& msg);
void flushLog();
long parseZPInt(const char *s);


void setup() {
	Serial.begin(115200); //or 57600 to be on the safe side?
	Serial.println(F("** PNEUMATIC SPONGE BALL ACCELERATOR SAYS HELLO **"));
	Serial.println();

	pinMode(airDirectionSensor, INPUT );
	pinMode(airDirectionMotor,  OUTPUT);
	pinMode(magnetValve,        OUTPUT);
	pinMode(stirMotorA,         OUTPUT);
	pinMode(stirMotorB,         OUTPUT);
	pinMode(relais1,            OUTPUT);
	pinMode(relais2,            OUTPUT);
	pinMode(relais3,            OUTPUT);
	pinMode(lightBarrierPin,    INPUT );
	attractModeTimer=millis();  // reset attractModeTimer

	initSdCard();
#ifdef NETWORK_ENABLED
	initEthernet();
#endif

	Serial.println(F("** setup done. **"));
}


bool initSdCard() {
	Serial.print(F("Initializing SD card... "));

	//On the Ethernet Shield, CS is pin 4. It's set as an output by default.
	//the hardware SS pin (10 on most Arduino boards) must be left as an output or the SD library functions will not work.
	pinMode(10, OUTPUT);

	if (!SD.begin(4)) {
		Serial.println(F("failed!"));
		return false;
	}
	Serial.println(F("done."));

	//Note: put logs in a directory since the root folder on fat16 systems can only hold 512 entries (and just over 65000 in total)
	// if (!SD.exists(LOG_DIR)) SD.mkdir(LOG_DIR);

	File logDir = SD.open(LOG_DIR);
	int maxIdx = -1;
	while (true) {
		File f = logDir.openNextFile();
		if (!f) break;

		int idx = parseZPInt(f.name());
		if (idx > maxIdx) maxIdx = idx;
		f.close();
	}
	maxIdx++;

	const int dirlen = strlen(LOG_DIR);
	char *fname = (char*)malloc(dirlen + 10); //'nnnnn.log'= 9 + 1 for nulbyte
	strcpy(fname, LOG_DIR);
	strcat(fname, "/00000.log");

	for (int i = dirlen + 5; i >= dirlen + 1 && maxIdx > 0; i--) {
		fname[i] = '0' + (char)(maxIdx % 10); //extract lowest digit and store in filename
		maxIdx /= 10; //shift out lowest digit to prepare for next loop iteration
	}
	
	Serial.print(F("Attempting to use log file: ")); Serial.println(fname);

	logFile = SD.open(fname, FILE_WRITE);
	free(fname);

	if (!logFile) logMessage(F("Error opening log file."));

	return true;
}

#ifdef NETWORK_ENABLED
bool initEthernet() {
	Serial.print(F("Initializing ethernet... "));
	delay(1000); //allow the ethernet module to boot
	Ethernet.begin(const_cast<uint8_t*>(MAC_ADDRESS), IP_ADDRESS, DNS_SERVER);
	Serial.print("Ethernet IP address: "); Serial.println(Ethernet.localIP());
}
#endif



#ifndef TEST_MODE
void loop() {
	static const unsigned long SHORT_SENSOR_TOUCH  = 2000L;

	int initialAirDirection = airFlowDirection;

	unsigned long prevSensorToggle;

	logMessage(F("[LPen,reg] === STARTING PROGRAM LOOP! ==="));
	logMessage(F("[LPwt] wait for sensor to be activated ..."));

	flushLog();

	while (!readHandSensor()) {//stay in this little loop until sensor is activated
		if (attractModeTimer+attractModeInterval < millis()) attractMode(); // switch to attractMode after certain time when nothing happened
	}
	prevSensorToggle = millis();

	pHandSensor = true;
	logMessage(F("[LPac] >> sensor activated!"));

	magnetValveTimer = millis(); //reset magnetValveTimer
	vandalismTimer   = millis(); //reset vandalismTimer

	switchInstallationOn();

	while (autoSwitchOffTimer+autoSwitchOffDelay > millis() ){ // LOOP : until sensor was not pushed for [timeout1] in milliseconds

		if (readHandSensor() != pHandSensor){                    // check if hand sensor was toggled
			logMessage(F("[LPtg] >> sensor toggled!"));
			pHandSensor = !pHandSensor;
			if (millis() - prevSensorToggle > SHORT_SENSOR_TOUCH) {
				reverseAirDirection();
			}
			vandalismTimer = millis();                             // reset vandalismTimer
			prevSensorToggle = lastSensorToggle;
		}
		magnetValveRoutine();
		checkLightBarrier();

#ifdef NETWORK_ENABLED
		readNetworkData();
#endif

		if (vandalismTimer+vandalismTimeOut < millis()){        // check if sensor was not toggled for a long time
			logMessage(F("[LPno] ** VANDALISM TIMEOUT -- SWITCHING OFF INSTALLATION BECAUSE SENSOR WAS NOT TOGGLED FOR A WHILE ! **"));
			switchInstallationOff();
			while (readHandSensor() == pHandSensor){}              // wait until sensor is toggled again
			break;                                                 // break out of LOOP
		}

		flushLog();
	}                                                          // end of : LOOP

	//Note: attempt to keep the system more full in order to prevent the 'hey nothing happens when I touch the hand'-situations
	if (airFlowDirection != initialAirDirection) {
		reverseAirDirection(); //do this extra reverse to make sure direction will be reversed on the next run
		interruptableDelay(3000);
	}
	reverseAirDirection(); //and now make the total count *actually* odd
	interruptableDelay(3000);
	switchInstallationOff();
	attractModeTimer=millis();  // reset attractModeTimer
}
//-----------------------------------------------------------------------------------

#elif TEST_MODE == 1
void loop() {
	logMessage(F("[LPen,T1] === STARTING TEST LOOP! (rev+wait) ==="));
	switchInstallationOn();
	delay(10000);

	magnetValveRoutine();
	switchInstallationOff();
	reverseAirDirection();
	delay(2000);
}
//-----------------------------------------------------------------------------------

#elif TEST_MODE == 2
void loop() {
	static bool switchedOn = false;

	logMessage(F("[LPen,T2] === STARTING TEST LOOP! (continuous+rev) ==="));

	if (!switchedOn) {
		switchInstallationOn();
		switchedOn = true;
	}

	delay(10000);

	magnetValveRoutine();
	reverseAirDirection();
}
//-----------------------------------------------------------------------------------

#elif TEST_MODE == 3
//untested
void loop() {
	static const unsigned long HS_WAIT_TIME = 10000L;
	static bool switchedOn = false;
	static bool prevHandSensor = false;
	static unsigned long handSensorOn = 0L;

	logMessage(F("[LPen,T3] === STARTING TEST LOOP! (continuous) ==="));
	
	if (!switchedOn) {
		switchInstallationOn();
		switchedOn = true;
	}

	delay(10000);
	magnetValveRoutine();

	bool hs = readHandSensor();
	if (!prevHandSensor && hs) {
		handSensorOn = millis();
	} else if (prevHandSensor && hs && millis() - handSensorOn >= HS_WAIT_TIME) {
		reverseAirDirection();
	}

	prevHandSensor = hs;
}
//-----------------------------------------------------------------------------------

#else
# error "Unknown test mode"
#endif


void attractMode(){
	logMessage(F("[AMen] +++ ATTRACT MODE +++"));
	switchInstallationOn();

	unsigned long attractTimeOut = millis() + ATTRACT_MODE_DURATION; // duration of attract mode
	unsigned long reverseTimeOut = millis() + ATTRACT_MODE_AIR_REV_DELAY; // time after which air direction is reversed

	while(attractTimeOut > millis()) {
		if (readHandSensor()) break;

		magnetValveRoutine();
		checkLightBarrier();

		if (millis() > reverseTimeOut) {
			reverseAirDirection();
			reverseTimeOut = millis() + random(ATTRACT_MODE_AIR_REV_MIN_ADD, ATTRACT_MODE_AIR_REV_MAX_ADD);
		}
	}

	if (readHandSensor()) return;
	switchInstallationOff();
	attractModeTimer = millis();  // reset attractModeTimer
}

void magnetValveRoutine(){
	if (magnetValveTimer+magnetValveInterval<millis()){
		logMessage(F("[MVop] ::: Pffft! ::: open magnet valve!"));
		for (int i=0;i<3;i++){
			digitalWrite(magnetValve,HIGH);
			delay(MAGNET_VALVE_BURST_TIME);
			digitalWrite(magnetValve,LOW);
			delay(MAGNET_VALVE_BURST_TIME);
		}
		digitalWrite(magnetValve,HIGH);
		delay(1000);
		digitalWrite(magnetValve,LOW);
		magnetValveTimer=millis();
	}
}

void resetAutoSwitchOffDelay (){
	autoSwitchOffTimer = millis();
}

bool readHandSensor(){
	static bool prevValue = false;

	bool newValue;
	if (analogRead(handSensor)>500){
		resetAutoSwitchOffDelay ();
		newValue = true;
	}else{
		newValue = false;
	}

	if (newValue != prevValue) {
		lastSensorToggle = millis();
	}

	prevValue = newValue;
	return newValue;
}

void reverseAirDirection(){
	String logMsg = F("[RDen] reverse air direction ... ");

	if (installationOn) {
		digitalWrite(relais1, LOW); //disable air flow
	}

	digitalWrite(airDirectionMotor, HIGH);
	delay(500);
	while(analogRead(airDirectionSensor)<500); // spin gear until sensor value >= 500
	digitalWrite(airDirectionMotor,LOW);
	airFlowDirection*=-1;
	
	if (installationOn) digitalWrite(relais1, HIGH); //re-enable air flow
	
	logMessage(logMsg + F("new direction=") + airFlowDirection);
}

void switchInstallationOn(){
	logMessage(F("[I1en] relais and stirring motors ON"));
	digitalWrite(relais1,HIGH);
	delay(500);
	digitalWrite(relais2,HIGH);
	delay(500);
	digitalWrite(relais3,HIGH);
	digitalWrite(stirMotorA,HIGH);
	digitalWrite(stirMotorB,HIGH);
	installationOn = true;
}

void switchInstallationOff(){
	logMessage(F("[I0en] relais and stirring motors OFF"));
	digitalWrite(stirMotorA,LOW);
	digitalWrite(stirMotorB,LOW);
	digitalWrite(relais1,LOW);
	delay(500);
	digitalWrite(relais2,LOW);
	delay(500);
	digitalWrite(relais3,LOW);
	installationOn = false;
}

void checkLightBarrier() {
	static bool lastState = 0;
	static unsigned long lastSwitchTime = 0;

	unsigned long now = millis();

	int v = analogRead(lightBarrierPin);
	bool s = v >= LIGHT_BARRIER_THRESHOLD ? true : false;

	if (lastState != s) {
		lastSwitchTime = now;
		if (!s) ballCounter++; //only count falling edges
	}

	if (installationOn && now - lastSwitchTime >= LIGHT_BARRIER_TIMEOUT) {
		logMessage(F("[LBto] light barrier switch timed out (timer reset)"));
		lastSwitchTime = now;
	}

	lastState = s;

	//log 
	static unsigned long lastValDump = 0;
	static unsigned long lastBallCounter = 0;

	if (now - lastValDump > 5000 && lastBallCounter < ballCounter) {
		//Serial.print(F("lbThresh: ")); Serial.print(s?"T":"F"); Serial.print(F(" count: ")); Serial.print(ballCounter); Serial.print(F(" lbVal: ")); Serial.println(v);
		String msg = F("[LBct,"); msg += ballCounter; msg += F("] ball counter");
		logMessage(msg);
		lastValDump = now;
		lastBallCounter = ballCounter;
	}
}

//returns true when interrupted, false otherwise
bool interruptableDelay(unsigned long msecs) {
	unsigned long until = millis() + msecs;

	while (millis() < until) if (readHandSensor()) return true;

	return false;
}


/* aux functions */

void logMessage(const String& msg) {
	const unsigned long now = millis();

	Serial.print(now);
	Serial.print("\t");
	Serial.println(msg);

	if (logFile) {
		logFile.print(now);
		logFile.print("\t");
		logFile.println(msg);
	}

#ifdef NETWORK_ENABLED
	sendNetworkData(msg);
#endif
}

//flushing is required to actually write out data to the SD card
void flushLog() {
	static unsigned long lastLogFlush = 0L;
	unsigned long now = millis();
	if (now - lastLogFlush > LOG_FLUSH_DELAY) {
		lastLogFlush = now;
		if (logFile) logFile.flush();
	}
}

#ifdef NETWORK_ENABLED
void readNetworkData() {
	//while (client.available()) client.read();
	Serial.println("---------- network data ----------"); //TEMP //TODO: this is pure spam in the log...
	while (client.available()) Serial.print(client.read());
	Serial.println();
	client.flush();
}

//Note: do not call logMessage here, to prevent endless recursion
void sendNetworkData(const String& msg) {
	if (!client.connected()) client.connect(SERVER_HOST, 80);
	// if (!client.connected()) return;

	Serial.println("[NDxm] Sending network data"); //TEMP

	String pkt = String() + F("GET /psba.php?op=log&msg=") + urlencode(msg) + F("&ts=") + millis() + F(" HTTP/1.1\n"); //start with empty string to get proper operator overloads
    pkt += F("Host: lsof.nl\n");
    pkt += F("User-Agent: arduino-ethernet\n");
    pkt += F("Connection: close\n\n");
    
    Serial.println("About to send: " + pkt); //TEMP
    client.print(pkt);
}
#endif /* NETWORK_ENABLED */


//adapted from parseInt in Arduino's Stream library, only parses Z+
long parseZPInt(const char *s) {
	long value = 0;
	const char *p = s;
	int c = *p;

	while (c >= '0' && c <= '9') {
		value = value * 10 + (c - '0');
		p++;
		c = *p;
	}

	return value;
}

//Note: adapted from here: http://hardwarefun.com/tutorials/url-encoding-in-arduino
String urlencode(const String& s) {
	static const char *HEX_CHARS = "0123456789abcdef";
	String out;
 
	for (int i = 0; i < s.length(); ++i) {
		char c = s.charAt(i);
		if (('a' <= c && c <= 'z')
		     || ('A' <= c && c <= 'Z')
		     || ('0' <= c && c <= '9')) {
			out += c;
		} else {
			out += '%';
			out += HEX_CHARS[c >> 4];
			out += HEX_CHARS[c & 15];
		}
	}

	return out;
}
